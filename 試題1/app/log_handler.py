import logging
import logging.config

class LogHandler:
    def __init__(self, log_level='INFO', service='DA_Project'):
        self.log_level = log_level
        self.service = service
        self.set_logging()

    def set_logging(self):
        logging.config.dictConfig(config=self._get_logging_config())

    def _get_logging_config(self):
        return {
            "version": 1,
            "disable_existing_loggers": True,
            "formatters": {
                "normal": {  # the name of formatter
                    "format": "%(asctime)s|%(levelname)s|{}|%(name)s|[%(pathname)s, %(lineno)s] : %(message)s ".format(self.service)
                }
            },
            "handlers": {
                "filehandler":{
                    'formatter': 'normal',
                    'class': 'logging.handlers.RotatingFileHandler',
                    'filename': './logs/service.log',
                    'mode': 'a',
                    'maxBytes': (100*1024*1024),
                    'backupCount':10
                }
            },
            "loggers": {
                "INIT": {  # the name of logger
                    "handlers": ["filehandler"],  # use the above "console1" and "console2" handler
                    "level": self.log_level  # logging level
                },
                "SERVICE": {
                    "handlers": ["filehandler"],
                    "level": self.log_level
                }
            }
        }
    def getlogger(self, type):
        return logging.getLogger(type)
