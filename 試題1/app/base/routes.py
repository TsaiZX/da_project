from flask import request, jsonify
from app.base import blueprint
from app import service_logger


def verifyPayload(req):
    if "a" not in req or "b" not in req:
        # lack parameter
        return False
    if not isinstance(req["a"], int) or not isinstance(req["b"], int):
        # wrong type
        return False
    return True


@blueprint.route("add/", methods=["POST"])
def add():
    req = request.get_json()
    service_logger.info("Request payload : %s"%(req))
    result = ""
    
    if not verifyPayload(req):
        # when request payload is invalid, return the information and logging it
        service_logger.error("Sorry, The request payload is invalid!")
        return "Sorry, The request payload is invalid!"
    else:
        # mainly function
        result = {
            "sum":req['a'] + req['b']
        }
        service_logger.info("Success ,sum is [%s]"%(result["sum"]))
        return jsonify(result)


    
