from flask import Blueprint
from app import init_logger

blueprint = Blueprint(
    'DB_PROJECT',
    __name__,
    url_prefix='/'
)

service_logger = init_logger.info('Service register success')