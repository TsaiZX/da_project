import os
from flask import Flask
from importlib import import_module
from app.log_handler import LogHandler

log_handler = LogHandler()
init_logger = log_handler.getlogger('INIT')
service_logger = log_handler.getlogger('SERVICE')


def register_blueprints(app):
    # register the application
    for module_name in ['base']:
        module = import_module('app.{}.routes'.format(module_name))
        app.register_blueprint(module.blueprint)

def create_app():
    app = Flask(__name__, static_folder='base/static')
    
    try: 
        register_blueprints(app)
        init_logger.info("Service build up successfully")
    except Exception as err:
        init_logger.critical("Service build up Failed: {}".format(err))
        os.kill(0,4)

    return app
