# DA_Project

## 開發環境說明
- ubuntu 20.04
- python 3.8
- docker client 1.0.29
- docker-compose 2.13.0

## 試題1
-   解題說明
    -   採用flask blueprint框架, blueprint是flask提供可以將不同功能的路由模組化管理, 可以按照不同面向採用不同的架構.
    -   test.py 為測試接口範例程式.
    -   LOG
        1. log 格式由log_handler定義,此定義方式可以方便後續追查問題,以及當多服務之間串流, 快速定位至是哪項服務
        2. 會將container內部的logs資料夾映射至外部, 方便後面log解析工具解析
        3. 當參數不符合預期, 在LOG當中會紀錄Error
    - 本地端直接執行不使用docker
        1. 進到試題1資料夾
        2. `gunicorn -w 5 --thread=2 -b 0.0.0.0:80 run:app`
    -   API 接口說明
        <table>
            <tr>
                <td>接口路徑</td>
                <td>/add</td>
            <tr>
            <tr>
                <td colspan="2" align='center'>參數說明</td>
            <tr>
            <tr>
                <td>a</td>
                <td>任一數值, type: int</td>
            <tr>
            <tr>
                <td>b</td>
                <td>任一數值, type: int</td>
            <tr>
            <tr>
                <td colspan="2" align='center'>輸出說明</td>
            <tr>
            <tr>
                <td colspan="2">若輸入參數不包含a、不包含b或者、a,b所輸入非int,接口會回應"Sorry, The request payload is invalid!", 並且Log當中會記錄error</td>
            <tr>
        </table>
-   操作說明
    - image build 指令
        1. 進到試題1資料夾
        2. `docker build -t sum_service .`
    - docker compose 指令新增PORT參數, 啟動指令如下
        1. 進到試題1資料夾
        2. 配置好.env, 內含所需服務需要配置的參數
        3. `PORT="8091" docker-compose up -d`

## 試題2
-   解題說明
    - 採用 filebeat + ELK, 因為皆部屬在同一 host 上, 所以 filebeat 與 ELK 之中沒有額外架設 MQ(kafka) 的機制. 也因為只有架設單一 node elasticsearch, 因此沒有使用 elasticsearch 的 lifecycle, 讓 log 定期清除
    - 將題目1中 service 的 log 資料夾透過 docker volumes 至 host 端,再將此資料夾透過 docker volumes 映射至 filebeat 內部, 最後藉由 filebeat 將資料打入至 logstash 解析 log, 再將資料發送至 elasticsearch.
    - 因為題目需要告警功能, Kibana community 版本僅支援 mail 的功能, 因此報表以及告警的部分改採用 Grafana, 藉由 Grafana 將警告發送至 LINE
-   操作說明
    1. 事先務必確認 sum_service 已在試題1當中將image建立好
    2. 進到試題2資料夾
    3. 配置好.env, 內含所需服務需要配置的參數
    4. `docker-compose up -d`